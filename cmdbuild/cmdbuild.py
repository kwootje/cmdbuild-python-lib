# module for CMDBuild interfacing
# -*- coding: utf-8 -*-
# Trying to follow https://www.python.org/dev/peps/pep-0008/
#
# Changelog
# 2015-09-23 J. Baten Initial version
#
# Copyright 2015 Deltares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
_cmdbuild__version = "$Id: 7737ac50a0b55d807cf054605a99e74190e8ad6f $"
__author__ = 'Jeroen Baten'
__copyright__ = "Copyright 2015, Deltares"

import requests
import json
from pprint import pprint,pformat
import logging
import sys
import urllib # for encoding CQL queries

# import sys

logger = logging.getLogger(__name__)


class cmdbuild:
    """CMDBuild interface class. Please see _dir_ for methods"""

    def version(self):
        """
        Method to print filename and version string
        :return:
        """
        return __file__ + " version: " + _cmdbuild__version

    def check_valid_json(self, myjson):
        """Function to check is object passed is valid json
        :param: myjson:  json object to check
        Returns: true if valid json, otherwise false
        """
        try:
            json_object = json.loads(myjson)
        except ValueError, e:
            return False
        return True

    def info(self):
        """Return version information."""
        return "CMDBuild python lib version:" + _cmdbuild__version

    def connect(self, url, user, password):
        """Method to authenticate to cmdbuild server

        :param: url: url of server.
        :param: user: username to authenticate as.
        :param: password: password to use when authenticating.

        Returns: 0 if succesfull or 1 for failure.
        """
        if not url:
            raise Exception('ERROR: No URL supplied')

        if not user:
            raise Exception('ERROR: No username supplied')

        if not password:
            raise Exception('ERROR: No password supplied')

        logging.debug("*** Login and get authentication token ")

        # It turns out that a '/' at the end of the url will make the authentication process fail.
        url=url.strip('/')

        # A piece of code to get extra logging information
        #
        #  try:
        #     import http.client as http_client
        # except ImportError:
        #     # Python 2
        #     import httplib as http_client
        # http_client.HTTPConnection.debuglevel = 1
        #
        # # You must initialize logging, otherwise you'll not see debug output.
        # logging.basicConfig()
        # logging.getLogger().setLevel(logging.DEBUG)
        # requests_log = logging.getLogger("requests.packages.urllib3")
        # requests_log.setLevel(logging.DEBUG)
        # requests_log.propagate = True

        cmdbuild_url = url + "/services/rest/v2/sessions/"
        data = {'username': user, 'password': password}
        headers = {'Content-type': 'application/json', 'Accept': '*/*'}
        try:
            r = requests.post(cmdbuild_url, data=json.dumps(data), headers=headers)
        except requests.exceptions.RequestException as e:    # This is the correct syntax
            print e
            sys.exit(1)
        if r.status_code <> 200:
            r.raise_for_status()
            sys.exit(1)
        # logging.debug((r.json()))
        r1 = r.json()
        sessionid = r1["data"]["_id"]
        # logging.debug("sessionid=" + str(pprint(sessionid)))
        # logging.info(" Authentication token : " + sessionid)

        if (len(str(sessionid)) > 1):
            self.url = url
            self.user = user
            self.password = password
            self.sessionid = sessionid
            return 0
        # else:
        #    return 1
        return 1

    def session_info(self):
        """
        :return: json object with information about the current session in json format

        """
        logging.debug("*** Session info")
        cmdbuild_url = self.url + "/services/rest/v2/sessions/" + self.sessionid
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def lookup_types_info(self):
        """
        Return list of defined lookup types

        :return: Lookup types found (in json format).
        """
        logging.debug("*** lookup_types")
        cmdbuild_url = self.url + "/services/rest/v2/lookup_types"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results")
        logging.debug(r.json())
        return r.json()

    def lookup_type_values(self, id):
        """
        Return values for given lookup type

        :param id: id of lookup type.
        :return: list of values found in json format.
        """
        logging.debug("\nTrying to find lookup values for : " + id)
        logging.debug("*** LookupType '" + id + "'")
        cmdbuild_url = self.url + "/services/rest/v2/lookup_types/" + id + "/values"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for lookup_type " + id + "?")
        logging.debug(r.json())
        return r.json()

    def lookup_type_details(self, name, id):
        """Return value for given lookup type id

        :param: name: name of lookup type
        :param: id:   id of lookup type value id.

        :return: All details of lookup type value in json format.
        """
        logging.debug("*** LookupTypeValue name'" + name + "'")
        logging.debug("*** LookupTypeValue id  '" + id + "'")
        cmdbuild_url = self.url + "/services/rest/v2/lookup_types/" + name + "/values/" + id
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def domains_list(self):
        """
        :return: list of domains defined
        """
        logging.debug("*** domains")
        cmdbuild_url = self.url + "/services/rest/v2/domains"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.info("There are " + str(r.json()["meta"]["total"]) + " results")
        logging.debug(r.json())
        return r.json()

    def domain_relations(self, id):
        """
        Return relations of specified domain as json object

        :param: id: id of requested domain
        :return: json object
        """
        logging.debug("*** Domain relations of id:'" + id + "'")
        cmdbuild_url = self.url + "/services/rest/v2/domains/" + id + "/relations/"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def domain_relations_with_filter(self, id1, field, value):
        """
        Return relations of specified domain as json object

        :param: id:    id of requested domain
        :param:  field: field to filter on
        :param:  value:   value used in filter
        :return: json object
        """
        logging.debug("*** Domains of type " + id1 + " where field '" + field + "' has value '" + str(value) + "'")
        filter = "{\"attribute\":{\"simple\":{\"attribute\":\"" + field + "\",\"operator\":\"equal\",\"value\":[\"" + str(
            value) + "\"]}}}"
        logging.debug("filter looks like: " + filter)
        cmdbuild_url = self.url + "/services/rest/v2/domains/" + id1 + "/relations?filter=" + filter
        # cmdbuild_url = self.url + "/services/rest/v2/domains/" + id1 + "/relations/"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def domain_relation_details(self, name, id1):
        """Return relation details of specified domain as json object

        :param: name:    name of domain
        :param: id:   id of requested domain relation

        """
        logging.debug("*** Domain relation details of name " + name + " and id " + str(id1))
        cmdbuild_url = self.url + "/services/rest/v2/domains/" + name + "/relations/" + str(id1)
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def domain_details(self, id):
        """
        Return details of specified domain

        :param: name:  id of domain
        :return: json object
        """
        logging.debug("*** Domain '" + id + "' details")
        cmdbuild_url = self.url + "/services/rest/v2/domains/" + id
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def domain_attributes(self, id):
        """Return attributes of specified domain

        :param: name:  id of domain
        :return: json object
        """
        logging.debug("*** Domain '" + id + "' attributes")
        cmdbuild_url = self.url + "/services/rest/v2/domains/" + id + "/attributes"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def domain_get_all_with_filter(self, field, value):
        """Return all cards of specified domain and user filter field=value as json object

        :param:     field:   field to filter on
        :param:     value:   value used in filter
        :return: json object
        """
        logging.debug("*** Domains where field '" + field + "' has value '" + str(value) + "'")
        filter = "{\"attribute\":{\"simple\":{\"attribute\":\"" + field + "\",\"operator\":\"equal\",\"value\":[\"" + str(
            value) + "\"]}}}"
        cmdbuild_url = self.url + "/services/rest/v2/domains?filter=" + filter
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for this domain ")
        logging.debug(r.json())
        return r.json()


    def domain_relation_get_all_with_filter(self, domain, field, value):
        """Return all cards of specified class and user filter field=value as json object

        Argument:
        :param:  domain:  domain type to apply filter on
        :param: field:   field to filter on
        :param: value:   value used in filter
        :return: json object
        """

        logging.debug("*** Domains '" + domain + "' where relation '" + field + "' has value '" + str(value) + "'")
        cmdbuild_url = self.url + "/services/rest/v2/domains/" + domain + "/relations?filter={\"attribute\":{\"simple\":{\"attribute\":\"" + field + "\",\"operator\":\"equal\",\"value\":[\"" + str(
            value) + "\"]}}}"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        # logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for this domain ")
        logging.debug(r.json())
        return r.json()

    def classes_list(self):
        """
        :return: json object with list of available classes
        """
        logging.debug("*** Classes ")
        cmdbuild_url = self.url + "/services/rest/v2/classes"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results")
        logging.debug(r.json())
        return r.json()

    def classes_total(self):
        """
        :return: json object with list of available classes
        """
        logging.debug("*** Classes ")
        cmdbuild_url = self.url + "/services/rest/v2/classes"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results")
        logging.debug(str(r.json()["meta"]["total"]))
        return (r.json()["meta"]["total"])

    def class_details(self, id1):
        """
        Return details of specified class as json object

        :param:  id:    id of requested class
        :return: json object
        """
        logging.debug("*** Class details of id:'" + id1 + "'")
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + id1
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def class_get_attributes_of_type(self, id):
        """
        Return attributes of specified class as json object

        :param: id:    id of requested class
        :return: json object
        """
        logging.debug("*** Class  '" + id + "' attributes")
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + id + "/attributes"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for class " + id + " attributes ")
        logging.debug(r.json())
        return r.json()

    def class_get_all_cards_of_type(self, typ):
        """
        Return all cards of specified class as json object

        :param: type:    type of requested class
        :return: json object
        """
        logging.debug("*** Class  of type '" + typ + "' cards")
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + typ + "/cards"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for class of type " + typ + " cards ")
        logging.debug(r.json())
        return r.json()

    def class_get_all_cards_of_type_with_filter(self, typ, field, value):
        """
        Return all cards of specified class and user filter field=value as json object

        :param:  type:    type of requested class
        :param:  field:   field to filter on
        :param: value:   value used in filter
        :return: json object
        """
        logging.debug(
            "*** Class  of type '" + typ + "' cards where field '" + field + "' has value '" + str(value) + "'")
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + typ + "/cards?filter={\"attribute\":{\"simple\":{\"attribute\":\"" + field + "\",\"operator\":\"equal\",\"value\":[\"" + str(
            value) + "\"]}}}"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for class of type " + typ + " cards ")
        logging.debug(r.json())
        return r.json()

    def class_get_all_cards_of_type_with_filter_like(self, typ, field, value):
        """
        Return all cards of specified class and user filter field like value as json object

        :param: type:    type of requested class
        :param: field:   field to filter on
        :param: value:   value used in filter
        :return: json object
        """
        logging.debug(
            "*** Class  of type '" + typ + "' cards where field '" + field + "' has value '" + str(value) + "'")
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + typ + "/cards?filter={\"attribute\":{\"simple\":{\"attribute\":\"" + field + "\",\"operator\":\"like\",\"value\":[\"" + str(
            value) + "\"]}}}"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for class of type " + typ + " cards ")
        logging.debug(r.json())
        return r.json()

    def class_get_all_cards_of_type_with_filter2(self, typ, filter):
        """
        Return all cards of specified class and user supplied filter as json object

        :param:  type:    type of requested class
        :param:  filter:   supplied filter string
        :Example:   If you want to filter on a  reference  then you can finter on a specific attribute:
                    {"attribute":{"simple":{"attribute":"{attribute_name}", "operator":"isnull"}}}

                    otherwise you can use a CQL filter:

                    {"CQL":"from {my_class} where Id in (/(select * from {my_filter})/)"}

                    or

                    "{\"attribute\":{ \"" + oper + "\" : [ "
                    + "{" + "\"simple\":{\"attribute\":\"" + field1 + "\",\"operator\":\"equal\",\"value\":[\"" + str(value1) + "\"]}}"
                    + ","
                   +"{" +"\"simple\":{\"attribute\":\"" + field2 + "\",\"operator\":\"contain\",\"value\":[\"" + str(value2) + "\"]}}"
                   + "] } }"

                   or

                   {"CQL":"from CI_RS_IFST_IPAD_IPV4 where Id in (/(select \\"Id\\" from only \\"CI_RS_IFST_IPAD_IPV4\\" ci where (select count(*) from \\"Map\\" m where  (\\"IdObj1\\"=ci.\\"Id\\" or \\"IdObj2\\"=ci.\\"Id\\") and m.\\"Status\\"=\'A\') > 1  and ci.\\"network\\" = \''+str(network)+'\' )/)"

                   or

                   {"CQL":"from CI_RS where Id in (/( select \"IdObj2\" from \"Map\" where \"IdObj1\" = 8327 and \"Status\" = '\''A'\'')/)"}

        :return: json object
        """
        logging.debug( "*** Class  of type '" + typ + "' cards with filter '" + filter + "'")
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + typ + "/cards?filter=" + str(filter)
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for class of type " + typ + " cards ")
        logging.debug(r.json())
        return r.json()


    def class_get_card_details(self, type, id1):
        """
        Return all cards of specified class as json object

        :param: type:    type of requested class
        :param: id:      id of requested card
        :return: json object
        """
        logging.debug("*** Class  '" + type + "' card details " + str(id1))
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + str(type) + "/cards/" + str(id1)
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def class_insert_card(self, cardtype, cardobject):
        """Insert card with name into cmdbuild

        :param: name:    name of card
        :param: object:  json object with all relevant parameters
        :return:
            id of object created in JSON format
            error message when second argument is not a valid JSON object
        """

        if (self.check_valid_json(cardobject)):
            logging.debug("Inserting card of type " + str(cardtype) + " and with object:" + str(pformat(json.dumps(cardobject))))
            cmdbuild_url = self.url + "/services/rest/v2/classes/" + str(cardtype) + "/cards/"
            headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
            try:
                r = requests.post(cmdbuild_url, data=cardobject, headers=headers)
                if not r.status_code // 100 == 2:
                    return "Error: Unexpected response {}" + str(pformat(r))
                logging.debug(pformat(r))
                return r.json()
            except requests.exceptions.RequestException as e:
                logging.debug('HTTP ERROR %s occured' % e.code)
                logging.debug(e)
                return e
        else:
            return "Second argument is not a valid JSON object"

    def create_relation(self, relationtype, cardobject):
        """Insert card with name into cmdbuild

        :param relationtype: Rleation to create
        :param cardobject: json string with data
        :return:
            id of object created in JSON format
            error message when second argument is not a valid JSON object
        """

        if (self.check_valid_json(cardobject)):
            logging.debug("Inserting relation of type " + str(relationtype) +
                          " and with object:" + str(pformat(json.dumps(cardobject))))
            cmdbuild_url = self.url + "/services/rest/v2/domains/" + \
                str(relationtype) + "/relations/"
            headers = {'Content-type': 'application/json',
                       'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
            print(cmdbuild_url)
            print(headers)
            try:
                r = requests.post(
                    cmdbuild_url, data=cardobject, headers=headers)
                if not r.status_code // 100 == 2:
                    return "Error: Unexpected response {}" + str(pformat(r))
                logging.debug(pformat(r))
                return r.json()
            except requests.exceptions.RequestException as e:
                logging.debug('HTTP ERROR %s occured' % e.code)
                logging.debug(e)
                return e
        else:
            return "Second argument is not a valid JSON object"




    def get_by_type_and_id(self, typ, id1):
        """
        Retrieve Card or Class by id

        :param: classid:    id of requested classtype
        :param: cardtype:   type of card to request
        :param: cardid:     id of requested card
        :return: json object
        """
        logging.debug("*** get Card(s) by type = " + str(typ) + " and id = " + str(id1))
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + str(typ) + "/cards/" + str(id1) + "/"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def get_by_domaintype_and_id(self, typ, id1):
        """
        Retrieve Card or Class by id

        :param: classid:    id of requested classtype
        :param: cardtype:   type of card to request
        :param: cardid:     id of requested card
        :return: json object
        """
        logging.debug("*** get domain(s) by type = " + str(typ) + " and id = " + str(id1))
        cmdbuild_url = self.url + "/services/rest/v2/domains/" + str(typ) + "/" + str(id1) + "/"
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug(r.json())
        return r.json()

    def findCardsOfTypeWithStringInField(self, typ, field, findstr):
        """
        Find all cards of specified type where the field contents are LIKE string

        :param type: Typeof card to look for
        :param field: Field that must be searched
        :param findstr:  string to search for
        :return: json string
        """

        # filter = '{"CQL":"from CI_RS where Id in (/( select \\"IdObj2\\" from \\"Map\\" where \\"IdObj1\\" = ' + str(cardid) + ' and \\"Status\\" = \'A\' )/)"}'
        #filter='{"CQL":"from CI_RS_IFST_SVR where Id in (/( select \"Id\" from \"CI_RS_IFST_SVR\" where \"Description\" like '\''tl-%'\'' )/)"}'
        filter = '{"CQL":"from ' + typ + ' where Id in (/( select \\"Id\\" from \\"' + typ + '\\" where \\"' + field + '\\" like \'' + findstr + '\'  )/)"}'
        filter= urllib.quote_plus(filter)
        logging.debug( "*** Class  of type '" + typ + "' cards with filter '" + filter + "'")
        cmdbuild_url = self.url + "/services/rest/v2/classes/" + typ + "/cards?filter=" + str(filter)
        headers = {'Content-type': 'application/json', 'Accept': '*/*', 'CMDBuild-Authorization': self.sessionid}
        r = requests.get(cmdbuild_url, headers=headers)
        if not r.status_code // 100 == 2:
            return "Error: Unexpected response {}".format(r)
        logging.debug("There are " + str(r.json()["meta"]["total"]) + " results for class of type " + typ + " cards ")
        logging.debug(r.json())
        return r.json()


